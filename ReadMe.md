The best security you can get in a web browser!
Allow active content to run only from sites you trust, and protect yourself against XSS and Clickjacking attacks, "Spectre", "Meltdown" and other JavaScript exploits.
Fx52? <a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/937fcddbc1a319b18e863e99206261db7b9b6ef26ed271b01009e6dd9d5a2235/https%3A//noscript.net/getit">https://noscript.net/getit</a>

<b>IMPORTANT</b>
<a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/e19d2306e457c98e6c3e03f52480e7206685d42d1b075ee88b77e44b1f1d3813/https%3A//forums.informaction.com/viewtopic.php%3Ff=7&amp;t=23974&amp;p=94778">A Basic <b>NoScript 10 Guide</b></a>

Still confused by NoScript 10's new UI?
Check this <a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/f5ad7f79289d1b2c198593e25708cba9ab4104a300b4ab49f98975c26ad4bc69/https%3A//blog.jeaye.com/2017/11/30/noscript/">user-contributed NoScript 10 primer</a>.
and this <a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/4e700d4e76086494b5553ff9e42d881037d3adcf6d64fefa11545d184fe3d8b5/https%3A//hackademix.net/2017/12/04/noscript-quantum-vs-legacy-in-a-nutshell-2/">NoScript 10 "Quantum" vs NoScript 5 "Classic" (or "Legacy") comparison</a>.

Winner of the "PC World World Class Award" and bundled with the Tor Browser, NoScript gives you with the best available protection on the web.

It allows JavaScript, Flash, Java and other executable content to run only from trusted domains of your choice, e.g. your home-banking site, mitigating remotely exploitable vulnerabilities including Spectre and Meltdown.

It  protects your "trust boundaries" against cross-site scripting attacks (XSS), cross-zone DNS rebinding / CSRF attacks (router hacking), and Clickjacking attempts, thanks to its unique ClearClick technology. 

Such a preemptive approach  prevents exploitation of security vulnerabilities (known and even unknown!) with no loss of functionality where you need it.
Experts do agree: Firefox is really safer with NoScript ;-)

FAQ: <a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/8a4a623eb90a9becd02eed9474b3d82f9e6a93cd27281b3c5a9346d4da3543e7/https%3A//noscript.net/faq">https://noscript.net/faq</a>
Forum: <a rel="nofollow" href="https://outgoing.prod.mozaws.net/v1/8a6b9e607349db8d83719e8645277b4597538997d7a8946bb1515a4ff3f79287/https%3A//noscript.net/forum">https://noscript.net/forum</a>
